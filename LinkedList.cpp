#include "LinkedList.h"
#include "String.h"
#include <stdio.h>
#include <iostream>

using namespace std;

LinkedList::LinkedList() : m_tete(NULL),PTR(NULL)
{
}
void LinkedList::add(String &element) // ajouter un �l�ment au tableau
{
	if(this->m_tete == NULL) // le cas ou il ya un seul element
	{
		cout << "tete = NULL " << endl;
		this->PTR = new Element[1];
		PTR->chaine = new String[1];
		PTR->chaine->set(element);
		PTR->ptr = NULL;
		m_tete = this->PTR;
	}
	else // le cas ou il ya plusieur 
	{
		Element *parc;
		parc = m_tete;
		while(parc->ptr != NULL) // pour avoir l'adresse du dernier element
		{
			parc = parc->ptr ;
		}
		// l'adresse du dernier element
		this->PTR = new Element[1];
		PTR->chaine = new String[1];
		PTR->chaine->set(element);
		PTR->ptr = NULL;
		parc->ptr = this->PTR;
		
	}
}
void LinkedList::print() //afficher les �l�ments de la liste .
{
	Element *parc;
	parc = this->m_tete ;
	if(parc == NULL)
		cout << "La liste est vide :" << endl;
	else
	{
		do
		{
			parc->chaine->affiche();
			parc = parc->ptr;
		}while(parc != NULL);
	}
}
void LinkedList::add(LinkedList &elements) // fusionner avec une autre liste.
{
	Element *parc;
	parc = this->m_tete;
	while(parc->ptr !=NULL)
	{
		parc = parc->ptr;
	}
	parc->ptr = elements.getMtete();
}
String LinkedList::get(int index) // r�cup�rer l��l�ment N� index.
{
	Element *parc;
	int elementNum = 1;
	parc = this->m_tete;
	while(parc->ptr != NULL)
	{
		elementNum++;
		parc = parc->ptr;
		if(elementNum == index)
			break;
		
	}
	return parc->chaine->substring(0,parc->chaine->lenght());

}
void LinkedList::set(int index, String element)// remplacer l��l�ment N� index
{
	Element *parc;
	int elementNum = 1;
	parc = this->m_tete;
	while(parc->ptr != NULL)
	{
		elementNum++;
		parc = parc->ptr;
		if(elementNum == index)
			break;
		
	}
}
String LinkedList::remove(int index) // supprimer et r�cup�rer l��l�ment N� index.
{
	 // si c'est le premier element de la liste : 
	Element *aSupprimer;
	String aReturn;
	if(index == 1)
	{
		aSupprimer = this->m_tete;
		aReturn = this->m_tete->chaine->substring(0, this->m_tete->chaine->lenght());
		this->m_tete = this->m_tete->ptr; // le chainage 
		delete[] aSupprimer;
		return aReturn;
	}
	else
	{
		Element *parc ;
		Element *aSupprimer;
		String aReturn;
		int elementNum = 1;
		parc = this->m_tete;
		while(parc->ptr->ptr != NULL)
		{
			elementNum++;
			if(elementNum == index)
				break;
			parc = parc->ptr;
		}
		aSupprimer = parc->ptr;
		aReturn = parc->ptr->chaine->substring(0,parc->ptr->chaine->lenght());
		parc->ptr = parc->ptr->ptr;
		delete[] aSupprimer;
		return aReturn;
	}
	
}
int LinkedList::getIndex(String &element) // chercher et retourner l�index d�un �l�ment dans le tableau.
{
	int elementNum = 1;
	Element *parc; 
	parc = this->m_tete;
	while(parc->ptr != NULL)
	{
		
		if(parc->chaine->substring(0,parc->chaine->lenght()) == element)
		{
			break;
		}
		else
		{	
			elementNum++;
			parc = parc->ptr;
		}	
	}

	return elementNum;
}
int LinkedList::size() // retourner la taille du tableau.
{
	Element *parc ;
	int nombreElement = 1;
	parc = this->m_tete;
	while(parc->ptr != NULL)
	{
		nombreElement++;
		parc = parc->ptr;
	}
	return nombreElement;
}
int LinkedList::tailleTabDouble()
{
	return this->tailleDouble;
}
int LinkedList::tailleTabInt()
{
	return this->tailleInt;
}
double* LinkedList::getDoubleVector() // pour retourner un tableau des r�els contenus dans le Vector.
{
	size_t size = 0;
	size_t i=0;
	double *returnTableau = (double*)malloc(size*sizeof(double));
	Element *parc;
	parc = this->m_tete;
	while(parc->ptr != NULL)
	{
		if(parc->chaine->testDouble())
		{
			size++;
			returnTableau = (double*)realloc(returnTableau,size*sizeof(double));
			returnTableau[i] = parc->chaine->doubleValue();
			i++;
		}
		parc = parc->ptr;
	}
	this->tailleDouble = size;
	return returnTableau;
}
int* LinkedList::getIntVector() // pour retourner un tableau des entier contenus dans le Vector.
{
	size_t size = 0;
	size_t i=0;
	int *returnTableau = (int*)malloc(size*sizeof(int));
	Element *parc;
	parc = this->m_tete;
	while(parc->ptr != NULL)
	{
		if(parc->chaine->testInt())
		{
			size++;
			returnTableau = (int*)realloc(returnTableau,size*sizeof(int));
			returnTableau[i] = parc->chaine->intValue();
			i++;
		}
		parc = parc->ptr;
	}
	this->tailleInt = size;
	return returnTableau;
}
LinkedList::Element* LinkedList::getMtete() // return l'adresse de la tete de la liste
{
	return (this->m_tete);
} 
LinkedList::Element::Element()
{	

}
LinkedList::Element::~Element()
{
	//cout << "Destructeur de Element : " << endl;
	//delete this->ptr;
	getchar();
}
LinkedList::~LinkedList(void)
{
	cout << "destructeur Link : " << endl;
	if(m_tete != NULL)
	{
		delete[] this->m_tete;
		//delete[]  this->PTR;
	}
}
