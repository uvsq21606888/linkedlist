#include "String.h"
#include <stdio.h>
#include <iostream>
#include <malloc.h>
#include <math.h>
using namespace std;
String::String() // constructeur sont parametre 
{
	m_value = (char*)malloc(2 * sizeof(char));
	m_value[0] = 'a';
	m_value[1] = 0; // ubligatoire pour montrer la fin de la chaine

}
String::String(char *value) // surcharge de constructeur ave *value
{
	m_value = (char*)malloc((strlen(value)+1)*sizeof(char));
	for(size_t i = 0;i<strlen(value); i++)
	{
		*(m_value+i) = *(value+i) ;
	}
	*(m_value+strlen(value))=0;
}
String::String(int size) // cr�e une chiane de caract�re avec un size donner
{
	m_value = (char*)malloc((size + 1)*sizeof(char));
	for (int i = 0; i<size; i++)
	{
		*(m_value+i) = ' ';
	}
	*(m_value+size) = 0;
}
String::String(String &s) // constructeur de copie 
{
	this->m_value = (char*)malloc((s.lenght()+1)*sizeof(char));
	for(int i=0;i<(s.lenght());i++)
	{
		*(this->m_value +i ) = s.get(i) ;
	}
	*(this->m_value + s.lenght()) = 0;
}
void String::affiche() // afficher le contenu de la chaine 
{
	cout << "val = " << m_value << endl; ;
}
int String::lenght() // retourner la taille de la chaine 
{
	return (strlen(this->m_value));
}
char String::get(int i) // retourne le caract�re du num i
{
	char c = *(m_value + i);
	return c;
}
void String::set(int i, char c) // remplace le caract�re du num i par le cara c
{
	*(m_value + i) = c;
}
void String::set(char *string)// qui remplace le contenu de la cha�ne avec la cha�ne � string �
{
	m_value = (char*)malloc((strlen(string) + 1)*sizeof(char));
	for (size_t i = 0; i<strlen(string); i++)
	{
		*(m_value + i) = *(string + i);
	}
	*(m_value + strlen(string)) = 0;
}
void String::set(String &string)// qui remplace le contenu de la cha�ne avec la cha�ne � string �
{
	m_value = (char*)malloc((string.lenght() + 1)*sizeof(char));
	for (int i = 0; i<string.lenght() ; i++)
	{
		*(m_value + i) = string.get(i);
	}
	*(m_value + string.lenght()) = 0;
}
void String::append(char *string) //concat�nation de � string �.
{
   size_t size = this->lenght() + strlen(string)+1 ;
   this->m_value = (char*)realloc(m_value,size*sizeof(char));
   for(size_t i=0,j=this->lenght(); i < strlen(string) ; i++,j++)
		*(m_value+j) = *(string+i); 
   *(m_value+(size-1)) = 0;
}
void String::toUpper()// convertir en majuscule.
{
	for(size_t i = 0;i<strlen(m_value);i++)
	{
		if( (int)*(m_value+i) >= 97 && (int)*(m_value+i) <= 122)
		{
			*(m_value+i) = ((int)*(m_value+i)) - 32 ;
		}
	}
}
void String::toLower()  // convertir en minuscule.
{
	for(size_t i = 0;i<strlen(m_value);i++)
	{
		if( (int)*(m_value+i) >= 65 && (int)*(m_value+i) <= 90)
		{
			*(m_value+i) = ((int)*(m_value+i)) +32 ;
		}
	}
}
void String::insert(int i, char *string)// insertion d�une cha�ne � string � � la position i.
{
	// reallocation : 
	//printf("reallocation :\n");
	int size = strlen(m_value)+strlen(string)+1  ;
	m_value = (char*)realloc(m_value,size*sizeof(char));
	// la copie en fin pour m_value
	//printf("la copie en fin :\n");
	int indice = strlen(m_value) -1;
	size_t j = size -2 ;
	while(indice >= i )
	{
		*(m_value+(j)) = *(m_value+(indice)) ;
		j--;indice--;
	}
	// la copie de la chaine a l'indice i :
	//printf("La copie a l'indice i : \n");
	j = 0;
	while(j<strlen(string))
	{
		*(m_value + i) = *(string + j);
		j++;i++;
	}
	*(m_value+(size-1)) = 0;
}
void String::insert(int i, String string) // insertion d�une cha�ne � string � � la position i.
{
	// reallocation : 
	//printf("reallocation :\n");
	int size = strlen(m_value)+string.lenght()+1  ;
	m_value = (char*)realloc(m_value,size*sizeof(char));
	// la copie en fin pour m_value
	//printf("la copie en fin :\n");
	int indice = strlen(m_value) -1;
	int j = size -2 ;
	while(indice >= i )
	{
		*(m_value+(j)) = *(m_value+(indice)) ;
		j--;indice--;
	}
	// la copie de la chaine a l'indice i :
	//printf("La copie a l'indice i : \n");
	j = 0;
	while(j<string.lenght())
	{
		*(m_value + i) = string.get(j);
		j++;i++;
	}
	*(m_value+(size-1)) = 0;
}
 String String::substring(int i, int j) // qui extrait la sous cha�ne situ�e entre i et j.
{
	int size = (j-i)+1;
	String s1(size);
	int indice = 0;
	while(i<=j)
	{
		s1.set(indice,this->get(i));
		i++;indice++;
	}
	
	return s1;
} 
 bool String::existeChaine(char *value,char *string,int i) // l'existence d'une chaine a dans b depuit un indice donner
{
	int cpt = 0;
	if(strlen(value)-i < strlen(string)) // Si la taille value value-i < taille string --> pas de traitement
	{
		return false ; 
	}
	else // le traitement
	{
		for(size_t l=0,j=i; l<strlen(string); l++, j++)
		{
			if(*(value+j) == *(string +l))
			{
				cpt++ ;
			}
			if(cpt == strlen(string))
			{
				return true;
			}
		}
		return false;
	}

}
int String::pos(char *string) //qui cherche si � string � est contenu dans la cha�ne et retourne sa premi�re occurrence. La m�thode retourne -1 si la sous cha�ne est introuvable.
{
	if(strlen(m_value) < strlen(string) ) // m_value inf a string => impossible de ce trouver dedant
	{
		return -1;
	}
	else
	{
		for(size_t i=0 ;i<strlen(m_value) ; i++)
		{
			if(existeChaine(m_value,string,i) == true)
			return i;
		}
		return -1;
	}
}
bool String::existeChaine(char *value,String string,int i) // l'existence d'une chaine a dans b depuit un indice donner
{
	int cpt = 0;
	if((size_t)strlen(value)-i < (size_t)string.lenght()) // Si la taille value value-i < taille string --> pas de traitement
	{
		return false ; 
	}
	else // le traitement
	{
		for(int l=0,j=i; l<string.lenght(); l++, j++)
		{
			if(*(value+j) == string.get(l))
			{
				cpt++ ;
			}
			if(cpt == string.lenght())
			{
				return true;
			}
		}
		return false;
	}

}
int String::pos(String string) //qui cherche si � string � est contenu dans la cha�ne et retourne sa premi�re occurrence. La m�thode retourne -1 si la sous cha�ne est introuvable.
{
	if((size_t)strlen(m_value) < (size_t)string.lenght() ) // m_value inf a string => impossible de ce trouver dedant
	{
		return -1;
	}
	else
	{
		for(size_t i=0 ;i<strlen(m_value) ; i++)
		{
			if(existeChaine(m_value,string,i) == true)
			return i;
		}
		return -1;
	}
}
int String::intDecimal(char i)
{
	return ((int)i - 48);
}
bool String::testInt() // return bool si oui ou nn c'est un int
{
	int compt = 0; 
	for(int i=0;i<this->lenght() ;i++)
	{
		if((int)m_value[i] >= 48 && (int)m_value[i] <= 57)
		{
			compt++;
		}
	}
	if(compt == this->lenght())
		return true;
	else
		return false;
		
}
int String::intValue() // permettant de retourner sous forme d�entier le contenu de la cha�ne si c�est un entier.
{
	if(this->testInt())
	{
		int size = this->lenght()-1; // taille de la chaine
		int entier = 0;
		int indice = 0; 
		if((int)m_value[0] == 45)
		{
			indice++;
		}
		for(int i=indice; i < this->lenght() ; i++)
		{
			entier += ((int)m_value[i] - 48) *pow(10.0,size);
			size--;
		}
		if((int)m_value[0] == 45)
		{
			return (entier*(-1));
		}
		else
			return entier;
	}
	else 
		return -1;
}
bool String::testDouble() // return bool si oui ou nn c'est un double
{
	int compt = 0; 
	for(int i=0;i<this->lenght() ;i++)
	{
		if((int)m_value[i] >= 48 && (int)m_value[i] <= 57)
		{
			compt++;
		}
				else if((int)m_value[i] == 46)
				{
					compt++;
				}
	}
	if(compt == this->lenght())
		return true;
	else
		return false;
		
}
double String::doubleValue()// permettant de retourner sous forme de r�el le contenu de la cha�ne si c�est un r�el.
{
	if(this->testDouble())
	{
		// �tape 1 : negative ou positive : 
		int indice =0;
		if((int)m_value[0] == 45)
		indice++;
		// �tape 2 : sortir le coter entier : 
		int i=indice;
		while((int)m_value[indice] != 46 )
		{	
			indice++;
		}
		String s1 = this->substring(i,indice - 1); // extraire la chaine du entier
		indice ++;
		double entier = 0;
		int puissance = 1;
		while(indice < this->lenght())
		{
			entier += ((int)m_value[indice] - 48) *pow((1/10.0),puissance);
			puissance++;indice++;
		}
		// resemblage des deux partie: 
		entier += s1.intValue();

		if((int)m_value[0] == 45 && entier != 0)
		{
			return (entier*(-1));
		}
		else
		return entier;
	}
	else
		return -1;
	
}
bool String::operator ==(String a) // surcharge de l'operator == pour les type String
{
	if(a.lenght() != this->lenght())
		return false;
	else
	{
		size_t cont = 0;
		for(size_t i=0; i<a.lenght(); i++)
		{
			if(a.get(i) == this->get(i) )
				cont++;
		}
		if(cont == a.lenght())
			return true;
		else
			return false;
	}
}
String String::operator=(String  a) // surcharge de l'operator + pour les type String
{
	int size = a.lenght(); 
	m_value = (char*)realloc(m_value,(size+1)*sizeof(char));
	for(int i=0; i < size ; i++)
	{
		*(m_value+i) = a.get(i);
	}
	*(m_value+size) = 0;
	return *this;
}String::~String()
{
	cout << "Le destructeur de string " << endl;
	if(m_value != NULL)
	{
		free(m_value);
		m_value = NULL;
	}
	
}