#pragma once
class String
{
public:
	String(); // constructeur son paramettre
	String(int size); // int size : pour la taille de la chaine
	String(char *value); // passer une autre chaine en argument 
	String(double i); // ajoute un double a la chaine (avec son code d�cimal)
	String(char a); // // ajouter un char 
	String(String &string); // constructeur de copie.
	int lenght(); // retourne le taille de la chaine .
	char get(int i); // retourner le caract�re d'indice i
	void set(int i, char c); // remplace le caract�re du num i par le cara c
	void set(char *string); // repmlace la chiane avec une nouvelle
	void set(String &string);//repmlace la chiane avec un nouveau objet string
	void append(char *string); //concat�nation de � string �.
	void toUpper(); // convertir en majuscule.
	void toLower() ; //convertir en minuscule.
	void insert(int i, char *string) ;// insertion d�une cha�ne � string � � la position i.
	void affiche(); // affichage de la chaine de notre classe
	void insert(int i, String string) ; // insertion d�une cha�ne � string � � la position i.
	String substring(int i, int j) ;// qui extrait la sous cha�ne situ�e entre i et j.
	int pos(char *string); // qui cherche si � string � est contenu dans la cha�ne et retourne sa premi�re occurrence. La m�thode retourne -1 si la sous cha�ne est introuvable.
	bool existeChaine(char *value,String string,int i);
	bool existeChaine(char *value,char *string,int i); // l'existence d'une chaine a dans b depuit un indice donner
	int pos(String string); // qui cherche si � string � est contenu dans la cha�ne et retourne sa premi�re occurrence. La m�thode retourne -1 si la sous cha�ne est introuvable.
	int intDecimal(char i);
	int intValue(); // permettant de retourner sous forme d�entier le contenu de la cha�ne si c�est un entier.
	double doubleValue();// permettant de retourner sous forme de r�el le contenu de la cha�ne si c�est un r�el.
    String operator=(String a); // surcharge de l'operator = pour les type String
	bool operator==(String a); // surcharge de l'operator == 
	bool testDouble(); // return bool si oui ou nn c'est un double
	bool testInt(); // return bool si oui ou nn c'est un int
	~String();
private:
	char *m_value;

};