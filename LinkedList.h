#pragma once
#include "String.h"
class LinkedList
{
public:
	
	LinkedList(); // constructeur de LinkedList .
	class Element // class imbriquer 
	{
		public : 
			Element(); // constructeur .
			~Element(); // destructeur .
			String *chaine;
			Element *ptr;
	};
	void add(String &element); // ajouter un �l�ment a la liste .
	void add(LinkedList &elements); // fusionner avec une autre liste.
	LinkedList::Element* getMtete(); // return l'adresse de la tete de la liste
	String get(int index); // r�cup�rer l��l�ment N� index.
	void set(int index, String element);// remplacer l��l�ment N� index
	String remove(int index); // supprimer et r�cup�rer l��l�ment N� index.
	int getIndex(String &element); // chercher et retourner l�index d�un �l�ment dans le tableau.
	int size(); // retourner la taille du tableau.
	void print(); //afficher les �l�ments de la liste .
	double* getDoubleVector(); // pour retourner un tableau des r�els contenus dans le Vector.
	int* getIntVector(); // pour retourner un tableau des entier contenus dans le Vector.
	int tailleTabDouble(); // return la taille du tableau de double
	int tailleTabInt(); // return la taille du tableau d'entier
	~LinkedList(void); // destructeur de LinkedList .
	
private :
	
	Element *m_tete;
	Element *PTR;
	int tailleDouble;
	int tailleInt;
};
