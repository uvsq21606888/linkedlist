#include <iostream>
#include <stdio.h>
#include "LinkedList.h"


using namespace std;
void main()
{	
	LinkedList l1;
	String s1("Mouad");
	String s2("El Hajjami");
	String s3("C++");
	l1.add(s1);
	l1.add(s2);
	l1.add(s3);
	l1.add(s2);
	l1.add(s1);
	l1.add(s3);
	l1.add(s1);
	l1.add(s2);
	l1.print();
	double *tab;
	tab = l1.getDoubleVector();
	for(size_t i=0; i< l1.tailleTabDouble() ;i++)
		cout << tab[0] << endl;
	getchar();
}